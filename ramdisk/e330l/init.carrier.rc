# Copyright (C) 2012 The Android Open Source Project
#
# IMPORTANT: Do not create world writable files or directories.
# This is a common source of Android security bugs.
#

on init

    export BOOTCLASSPATH /system/framework/core.jar:/system/framework/core-junit.jar:/system/framework/bouncycastle.jar:/system/framework/ext.jar:/system/framework/framework.jar:/system/framework/framework2.jar:/system/framework/telephony-common.jar:/system/framework/voip-common.jar:/system/framework/mms-common.jar:/system/framework/android.policy.jar:/system/framework/services.jar:/system/framework/apache-xml.jar:/system/framework/sec_edm.jar:/system/framework/seccamera.jar:/system/framework/scrollpause.jar:/system/framework/stayrotation.jar:/system/framework/smartfaceservice.jar:/system/framework/secocsp.jar:/system/framework/abt-persistence.jar:/system/framework/sc.jar:/system/framework/oem-services.jar:/system/framework/commonimsinterface.jar
    
# Support Preload install apks
    mkdir /preload 0771 system system

on early-boot
    chown bluetooth net_bt_stack /dev/ttyHS0
    chmod 0660 /dev/ttyHS0


on boot

# permissions for bluetooth.
    setprop ro.bt.bdaddr_path "/efs/bluetooth/bt_addr"
    chown bluetooth net_bt_stack ro.bt.bdaddr_path
    chmod 0660 /sys/class/rfkill/rfkill0/state
    chown bluetooth net_bt_stack /sys/class/rfkill/rfkill0/state
    chown bluetooth net_bt_stack /sys/class/rfkill/rfkill0/type

# bluetooth LPM
    chmod 0660 /proc/bluetooth/sleep/lpm
    chmod 0220 /proc/bluetooth/sleep/btwrite
    chown bluetooth net_bt_stack /proc/bluetooth/sleep/lpm
    chown bluetooth net_bt_stack /proc/bluetooth/sleep/btwrite
    chmod 0600 /dev/btlock
    chown bluetooth bluetooth /dev/btlock

    write /proc/sys/vm/swappiness 130

# Permissions for InputDevices.
    chmod 0660 /sys/class/input/input2/enabled
    chmod 0660 /sys/class/input/input16/enabled
    chown system system /sys/class/input/input2/enabled
    chown system system /sys/class/input/input16/enabled

# tdmb
    chown system system /dev/tdmb
    chmod 0660 /dev/tdmb

# reset_reason
    chown system system /proc/reset_reason
    chmod 0600 /proc/reset_reason

# Permissions for usb_lock
    chown system radio /sys/class/sec/switch/.usb_lock/enable

# service for TZPR provisioning version check app
service scranton_RD /system/bin/scranton_RD
    class main
    user root
    disabled
    oneshot

# bluetooth dhcp config
    service dhcpcd_bt-pan /system/bin/dhcpcd -ABKL
    class main
    disabled
    oneshot

service iprenew_bt-pan /system/bin/dhcpcd -n
    class late_start
    disabled
    oneshot

# start for TZPR provisioning version check app
on property:sys.qseecomd.enable=true
    start scranton_RD

on fs

# For Absolute Persistence Partition
    mkdir /persdata 0755 system system
    mkdir /persdata/absolute 0750 system system

    wait /dev/block/platform/msm_sdcc.1/by-name/persdata
    check_fs /dev/block/platform/msm_sdcc.1/by-name/persdata ext4
    mount ext4 /dev/block/platform/msm_sdcc.1/by-name/persdata /persdata/absolute nosuid nodev barrier=1
    chown system system /persdata
    chmod 0755 /persdata
    chown system system /persdata/absolute
    chmod 0750 /persdata/absolute  

on post-fs-data
# MOBILE OFFICE Permanent memory access
    mkdir /efs/security 0771 root root
    chown radio system /efs/security
    chmod 0771 /efs/security

on post-fs
# for LGU I-WLAN
    insmod /system/lib/modules/mwlan_aarp.ko

on post-fs-data
# LGT contents directory
    mkdir /data/data/com.ubivelox.wipiplayer/W 0770 system lgt_gid
    mkdir /data/LGTContents 0770 system lgt_gid
    mkdir /data/LGTContents/video 0770 system lgt_gid
    mkdir /data/LGTContents/music 0770 system lgt_gid
    mkdir /data/LGTContents/bell 0770 system lgt_gid
    mkdir /data/LGTContents/app 0770 system lgt_gid
    mkdir /data/LGTContents/DLtmp 0770 system lgt_gid

    chown system lgt_gid /data/data/com.ubivelox.wipiplayer/W
    chown system lgt_gid /data/LGTContents
    chown system lgt_gid /data/LGTContents/video
    chown system lgt_gid /data/LGTContents/music
    chown system lgt_gid /data/LGTContents/bell
    chown system lgt_gid /data/LGTContents/app
    chown system lgt_gid /data/LGTContents/DLtmp

service dmb /system/bin/dmbserver
    class main
    user system
    group radio inet misc audio camera graphics net_bt net_bt_admin sdcard_rw sdcard_r shell

    restorecon /dev/icd
    restorecon /dev/icdr

# Support Preload install apks
on property:persist.sys.storage_preload=1
    mount ext4 /dev/block/platform/msm_sdcc.1/by-name/hidden /preload nosuid nodev ro barrier=1
    setprop storage.preload.complete 1

on property:persist.sys.storage_preload=0
    exec /system/bin/umount /preload

service swapon /sbin/sswap -s
    class core
    user root
    group root
    oneshot
